#!/bin/sh

set -e
set -u

docker-compose -f "{{ languageTool.paths.docker }}" pull || true
docker-compose -f "{{ languageTool.paths.docker }}" up -d
docker-compose -f "{{ languageTool.paths.docker }}" logs -f
