# Language Tool

Install language tools as docker on local

## Extensions

- [Chrome](https://chrome.google.com/webstore/detail/languagetool/oldceeleldhonbafppcapldpdifcinji)
- [Firefox](https://addons.mozilla.org/firefox/addon/languagetool?src=external-ltp-homepage)

## Install

Use role in ansible

## Start

```console
languageTool
```

## Configures languageTools extension

1. Open extension configuration
   - [Chrome](chrome-extension://oldceeleldhonbafppcapldpdifcinji/options/options.html?ref=popup-icon)
   - [Firefox](moz-extension://20b536c4-8fd0-4cfb-9e68-a652a7b16b8a/options/options.html?ref=popup-icon)
1. At bottom, unfold _Experimental settings (only for advanced users)_
1. Tick _Other server - requires LanguageTool server running there_
1. Enter : `http://127.0.0.1:9090/v2`
